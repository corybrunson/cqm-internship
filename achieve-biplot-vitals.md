# Tidy ordination and \newline clinical data analysis

<!--
To render HTML, install Pandoc and execute the following:
pandoc achieve-biplot-vitals.md -o achieve-biplot-vitals.html
-->

## Motivation

_Ordination_ encompasses a wide range of exploratory multivariate data analysis tools used for dimension reduction, feature extraction, visualization, and compression, as well as for hypothesis testing in certain settings. Among these tools are principal components analysis, correspondence analysis, and multidimensional scaling.
Probably the most common use of ordination is to produce two-layered scatterplots called _biplots_, which represent high-dimensional data on a small number of artificial coordinates, usually the first two of an ordination.
Numerous ordination and biplot methods are implemented in R but use inconsistent terminology and functionality. To enable researchers to perform ordination and create biplots more efficiently, a comprehensive software tool with a consistent interface and style is needed. The **tidyverse** library provides a natural framework for such a tool.

The field of health informatics seeks to improve medical research and practice by taking advantage of information contained in administrative databases, including EHRs and billing claims. While security, privacy, and liability concerns have introduced challenges and imposed constraints, several resources are available for research use.
The MIMIC database, for example, is an open-access EHR database intended for clinical research and education. MIMIC consists of real-world data from various sources on tens of thousands of patients admitted to intensive care units at Beth Israel Deaconess Medical Center.
Previous research has used MIMIC to characterize intensive care patient populations using recorded diagnoses, laboratory test results, time series of physiological measures, unstructured information contained in clinical notes, and demographic information.
While the objective of this research is often to predict patient outcomes such as readmission and mortality rates, a great deal of work goes into developing and honing exploratory tools.

The goals of this project are

1. to build an R package that consolidates a wide range of ordination methods into a single well-organized workflow based on tidy data principles, and
2. to design workflows using these methods and this package to analyze clinical data.

## Internship

### Preparation

- R
    - Install [R](https://www.r-project.org/) and [RStudio](https://www.rstudio.com/).
    - Practice R, e.g. at [DataCamp](https://www.jstatsoft.org/article/view/v059i10).
    - Read about [tidy data principles](https://www.jstatsoft.org/article/view/v059i10).
    - Install the [**tidyverse** suite of packages](https://www.tidyverse.org/).
    - Practice data manipulation and visualization following tutorials for [tidy data principles (**tibble**)](https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html), [the grammar of data manipulation (**dplyr**)](https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html), and [the grammar of graphics (**ggplot2**)](https://cfss.uchicago.edu/dataviz_grammar_of_graphics.html).
- Git
    - Install [Git](https://git-scm.com/) and practice tracking documents and commiting changes.
    - Set up accounts on [GitHub](https://github.com/) and [Bitbucket](https://bitbucket.org/). Practice creating, cloning, and communicating with remote repositories.
- _NAMCS_
    - _Read [about how NAMCS is conducted](https://www.cdc.gov/nchs/ahcd/about_ahcd.htm) and view the survey form and documentation for 2011._
    - _Download the 2011 dataset, read it into R, and restrict to the 13 chronic disorder variables to obtain a $0,1$-matrix._
- _MIMIC_
    - _Read [the published overview](https://www.nature.com/articles/sdata201635) of MIMIC-III and [the website documentation](https://mimic.physionet.org/) for several tables._
    - _Complete the [necessary coursework](https://mimic.physionet.org/gettingstarted/access/) to access the database for research purposes._
    - _Follow [the online instructions](https://mimic.physionet.org/gettingstarted/demo/) to download the Demo dataset._

### Topics

1. Collaborative and grammatical data analysis
    - Summary statistics, visualization, and modeling exercises
    - Version control with Git and GitHub
    - _Pre-processing and analysis NAMCS chronic conditions_
    - _Pre-processing and PDX analysis with MIMIC-III admissions_
2. Theory and practice of ordination
    - General concepts and terminology from Greenacre
    - Suitable ordinations of small datasets using R: `stats::prcomp()`, `stats::cmdscale()`, `MASS::lda()`, `ca::ca()`, `NMF::nmf()`
    - _LSVD/LPCA of NAMCS chronic conditions_
    - _Access to full MIMIC-III dataset through CITI_
    - _CA of MIMIC-III admissions (unit versus PDX)_
3. Ordination with **tidybiplot**
    - Guidance and exercises from documentation
    - Reproduce case studies and map workflows
    - Identify bugs, unintuitive behaviors, and needed functionality

### Deliverables

1. Implement **tidybiplot** methods for new ordination objects:
    - singular value decomposition (`svd()`)
    - principal components analysis (`stats::prcomp()`, `stats::princomp()`, `psych::principal()`)
    - linear discriminant analysis (`MASS::lda()`)
    - correspondence analysis (`ca::ca()`)
2. Implement new `ggbiplot()` layers:
    - unit circle
    - centroid
    - chull
    - ellipse
3. Vignettes or tutorials:
    - extracting and interpreting the components from an ordination object
    - comparing different ordinations on a common dataset (e.g. PCA versus MDS)
    - building a biplot with several layers

### Format

- Environment
    - CQM is restricted by badge access except on weekdays from 7:45 to 17:15.
    - You will have a workstation with a computer but you may also bring your own laptop.
    - You may bring your own lunch (and use our kitchen facilities) or go with one of us to the cafeteria in the main building.
    - Either Kathy, Reinhard, Sherli, or i will be available while you're here. Feel free to meet with us at any time for any reason.
- Schedule
    - Up to 150 hours (1 credit)
    - Approximately arranged as six 25-hour weeks
    - 6-hour workdays, 9:00–15:00: Monday | Tuesday | Thursday | Friday
    - Weeks: 25–29 Jul | 2–6 Jul | 9–10 + 26–27 Jul | 30 Jul–3 Aug | 6–10 Aug | 13–17 Aug
- Workflow
    - Begin and end work on time.
    - Set goals at the start of each week.
    - Create new branches to make changes, especially new functionality.
    - Discuss merging any branches to `master`.
- Documentation
    - Take photos!
    - Each fortnight, prepare a slideshow summarizing accomplishments, complications, and next steps.
    - Prepare a poster showcasing your work, to present to our colleagues at the end of the internship.

## Resources

### Methods

- Ordination
    - Orloci L (1966) Geometric Models in Ecology: I. The Theory and Application of Some Ordination Methods: [PDF](https://www.jstor.org/stable/2257667)
    - Podani J (2000) _Introduction to the Exploration of Multivariate Biological Data_: [PDF](http://ramet.elte.hu/~podani/subindex.html)
- Biplots
    - Gabriel KR (1971) The biplot graphic display of matrices with application to principal component analysis: [HTML/PDF](https://academic.oup.com/biomet/article-abstract/58/3/453/233361)
    - Greenacre M (2010) _Biplots in Practice_: [HTML](http://www.multivariatestatistics.org/biplots.html), [PDF](https://www.fbbva.es/wp-content/uploads/2017/05/dat/DE_2010_biplots_in_practice.pdf)

### Software

- R
    - [Getting Help with R](https://www.r-project.org/help.html)
    - [#rstats Twitter](https://twitter.com/hashtag/rstats)
    - [RStudio cheat sheets](https://www.rstudio.com/resources/cheatsheets/)
    - Wickham H & Grolemund G (2017) _R for Data Science_: [ebook](http://r4ds.had.co.nz/)
    - [Tidyverse style guide](http://style.tidyverse.org/)
- Git
    - [Git Reference](https://git-scm.com/docs)
    - [Resources to learn Git](https://try.github.io/)
- Markdown
    - [Markdown: Syntax](https://daringfireball.net/projects/markdown/syntax)
    - [Pandoc User's Guide](https://pandoc.org/MANUAL.html)

### Datasets

- The diabetes data used by [Reaven and Miller (1979)](https://link.springer.com/article/10.1007/BF00423145) can be obtained from the [**heplots** package](https://cran.r-project.org/web/packages/candisc/vignettes/diabetes.html).
- The [UCI Machine Learning Repository](http://archive.ics.uci.edu/ml/index.php) contains several healthcare-related datasets:
    - [Primary Tumor Data Set](https://archive.ics.uci.edu/ml/datasets/Primary+Tumor): categorical and logical variables
    - [Breast Cancer Data Set](https://archive.ics.uci.edu/ml/datasets/Breast+Cancer): categorical and ordinal variables
    - [Breast Tissue Data Set](https://archive.ics.uci.edu/ml/datasets/Breast+Tissue): categorical and continuous variables
    - [Dermatology Data Set](https://archive.ics.uci.edu/ml/datasets/Dermatology): discrete scale variables
    - [Hepatitis Data Set](https://archive.ics.uci.edu/ml/datasets/Hepatitis): logical variables
    - [Heart Disease Data Set](https://archive.ics.uci.edu/ml/datasets/Heart+Disease): heterogeneous variables
- The National Ambulatory Medical Care Survey [at the AHRQ website](https://www.cdc.gov/nchs/ahcd/index.htm) contains logical indicators for several chronic conditions, as well as several other variables.
- [MIMIC-III](https://mimic.physionet.org/) is a critical care database consolidated from several sources of administrative healthcare data.
