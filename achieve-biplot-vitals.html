<h1 id="tidy-ordination-and-clinical-data-analysis">Tidy ordination and clinical data analysis</h1>
<!--
To render HTML, install Pandoc and execute the following:
pandoc achieve-biplot-vitals.md -o achieve-biplot-vitals.html
-->
<h2 id="motivation">Motivation</h2>
<p><em>Ordination</em> encompasses a wide range of exploratory multivariate data analysis tools used for dimension reduction, feature extraction, visualization, and compression, as well as for hypothesis testing in certain settings. Among these tools are principal components analysis, correspondence analysis, and multidimensional scaling. Probably the most common use of ordination is to produce two-layered scatterplots called <em>biplots</em>, which represent high-dimensional data on a small number of artificial coordinates, usually the first two of an ordination. Numerous ordination and biplot methods are implemented in R but use inconsistent terminology and functionality. To enable researchers to perform ordination and create biplots more efficiently, a comprehensive software tool with a consistent interface and style is needed. The <strong>tidyverse</strong> library provides a natural framework for such a tool.</p>
<p>The field of health informatics seeks to improve medical research and practice by taking advantage of information contained in administrative databases, including EHRs and billing claims. While security, privacy, and liability concerns have introduced challenges and imposed constraints, several resources are available for research use. The MIMIC database, for example, is an open-access EHR database intended for clinical research and education. MIMIC consists of real-world data from various sources on tens of thousands of patients admitted to intensive care units at Beth Israel Deaconess Medical Center. Previous research has used MIMIC to characterize intensive care patient populations using recorded diagnoses, laboratory test results, time series of physiological measures, unstructured information contained in clinical notes, and demographic information. While the objective of this research is often to predict patient outcomes such as readmission and mortality rates, a great deal of work goes into developing and honing exploratory tools.</p>
<p>The goals of this project are</p>
<ol style="list-style-type: decimal">
<li>to build an R package that consolidates a wide range of ordination methods into a single well-organized workflow based on tidy data principles, and</li>
<li>to design workflows using these methods and this package to analyze clinical data.</li>
</ol>
<h2 id="internship">Internship</h2>
<h3 id="preparation">Preparation</h3>
<ul>
<li>R
<ul>
<li>Install <a href="https://www.r-project.org/">R</a> and <a href="https://www.rstudio.com/">RStudio</a>.</li>
<li>Practice R, e.g. at <a href="https://www.jstatsoft.org/article/view/v059i10">DataCamp</a>.</li>
<li>Read about <a href="https://www.jstatsoft.org/article/view/v059i10">tidy data principles</a>.</li>
<li>Install the <a href="https://www.tidyverse.org/"><strong>tidyverse</strong> suite of packages</a>.</li>
<li>Practice data manipulation and visualization following tutorials for <a href="https://cran.r-project.org/web/packages/tidyr/vignettes/tidy-data.html">tidy data principles (<strong>tibble</strong>)</a>, <a href="https://cran.r-project.org/web/packages/dplyr/vignettes/dplyr.html">the grammar of data manipulation (<strong>dplyr</strong>)</a>, and <a href="https://cfss.uchicago.edu/dataviz_grammar_of_graphics.html">the grammar of graphics (<strong>ggplot2</strong>)</a>.</li>
</ul></li>
<li>Git
<ul>
<li>Install <a href="https://git-scm.com/">Git</a> and practice tracking documents and commiting changes.</li>
<li>Set up accounts on <a href="https://github.com/">GitHub</a> and <a href="https://bitbucket.org/">Bitbucket</a>. Practice creating, cloning, and communicating with remote repositories.</li>
</ul></li>
<li><em>NAMCS</em>
<ul>
<li><em>Read <a href="https://www.cdc.gov/nchs/ahcd/about_ahcd.htm">about how NAMCS is conducted</a> and view the survey form and documentation for 2011.</em></li>
<li><em>Download the 2011 dataset, read it into R, and restrict to the 13 chronic disorder variables to obtain a <span class="math inline">0, 1</span>-matrix.</em></li>
</ul></li>
<li><em>MIMIC</em>
<ul>
<li><em>Read <a href="https://www.nature.com/articles/sdata201635">the published overview</a> of MIMIC-III and <a href="https://mimic.physionet.org/">the website documentation</a> for several tables.</em></li>
<li><em>Complete the <a href="https://mimic.physionet.org/gettingstarted/access/">necessary coursework</a> to access the database for research purposes.</em></li>
<li><em>Follow <a href="https://mimic.physionet.org/gettingstarted/demo/">the online instructions</a> to download the Demo dataset.</em></li>
</ul></li>
</ul>
<h3 id="topics">Topics</h3>
<ol style="list-style-type: decimal">
<li>Collaborative and grammatical data analysis
<ul>
<li>Summary statistics, visualization, and modeling exercises</li>
<li>Version control with Git and GitHub</li>
<li><em>Pre-processing and analysis NAMCS chronic conditions</em></li>
<li><em>Pre-processing and PDX analysis with MIMIC-III admissions</em></li>
</ul></li>
<li>Theory and practice of ordination
<ul>
<li>General concepts and terminology from Greenacre</li>
<li>Suitable ordinations of small datasets using R: <code>stats::prcomp()</code>, <code>stats::cmdscale()</code>, <code>MASS::lda()</code>, <code>ca::ca()</code>, <code>NMF::nmf()</code></li>
<li><em>LSVD/LPCA of NAMCS chronic conditions</em></li>
<li><em>Access to full MIMIC-III dataset through CITI</em></li>
<li><em>CA of MIMIC-III admissions (unit versus PDX)</em></li>
</ul></li>
<li>Ordination with <strong>tidybiplot</strong>
<ul>
<li>Guidance and exercises from documentation</li>
<li>Reproduce case studies and map workflows</li>
<li>Identify bugs, unintuitive behaviors, and needed functionality</li>
</ul></li>
</ol>
<h3 id="deliverables">Deliverables</h3>
<ol style="list-style-type: decimal">
<li>Implement <strong>tidybiplot</strong> methods for new ordination objects:
<ul>
<li>singular value decomposition (<code>svd()</code>)</li>
<li>principal components analysis (<code>stats::prcomp()</code>, <code>stats::princomp()</code>, <code>psych::principal()</code>)</li>
<li>linear discriminant analysis (<code>MASS::lda()</code>)</li>
<li>correspondence analysis (<code>ca::ca()</code>)</li>
</ul></li>
<li>Implement new <code>ggbiplot()</code> layers:
<ul>
<li>unit circle</li>
<li>centroid</li>
<li>chull</li>
<li>ellipse</li>
</ul></li>
<li>Vignettes or tutorials:
<ul>
<li>extracting and interpreting the components from an ordination object</li>
<li>comparing different ordinations on a common dataset (e.g. PCA versus MDS)</li>
<li>building a biplot with several layers</li>
</ul></li>
</ol>
<h3 id="format">Format</h3>
<ul>
<li>Environment
<ul>
<li>CQM is restricted by badge access except on weekdays from 7:45 to 17:15.</li>
<li>You will have a workstation with a computer but you may also bring your own laptop.</li>
<li>You may bring your own lunch (and use our kitchen facilities) or go with one of us to the cafeteria in the main building.</li>
<li>Either Kathy, Reinhard, Sherli, or i will be available while you're here. Feel free to meet with us at any time for any reason.</li>
</ul></li>
<li>Schedule
<ul>
<li>Up to 150 hours (1 credit)</li>
<li>Approximately arranged as six 25-hour weeks</li>
<li>6-hour workdays, 9:00–15:00: Monday | Tuesday | Thursday | Friday</li>
<li>Weeks: 25–29 Jul | 2–6 Jul | 9–10 + 26–27 Jul | 30 Jul–3 Aug | 6–10 Aug | 13–17 Aug</li>
</ul></li>
<li>Workflow
<ul>
<li>Begin and end work on time.</li>
<li>Set goals at the start of each week.</li>
<li>Create new branches to make changes, especially new functionality.</li>
<li>Discuss merging any branches to <code>master</code>.</li>
</ul></li>
<li>Documentation
<ul>
<li>Take photos!</li>
<li>Each fortnight, prepare a slideshow summarizing accomplishments, complications, and next steps.</li>
<li>Prepare a poster showcasing your work, to present to our colleagues at the end of the internship.</li>
</ul></li>
</ul>
<h2 id="resources">Resources</h2>
<h3 id="methods">Methods</h3>
<ul>
<li>Ordination
<ul>
<li>Orloci L (1966) Geometric Models in Ecology: I. The Theory and Application of Some Ordination Methods: <a href="https://www.jstor.org/stable/2257667">PDF</a></li>
<li>Podani J (2000) <em>Introduction to the Exploration of Multivariate Biological Data</em>: <a href="http://ramet.elte.hu/~podani/subindex.html">PDF</a></li>
</ul></li>
<li>Biplots
<ul>
<li>Gabriel KR (1971) The biplot graphic display of matrices with application to principal component analysis: <a href="https://academic.oup.com/biomet/article-abstract/58/3/453/233361">HTML/PDF</a></li>
<li>Greenacre M (2010) <em>Biplots in Practice</em>: <a href="http://www.multivariatestatistics.org/biplots.html">HTML</a>, <a href="https://www.fbbva.es/wp-content/uploads/2017/05/dat/DE_2010_biplots_in_practice.pdf">PDF</a></li>
</ul></li>
</ul>
<h3 id="software">Software</h3>
<ul>
<li>R
<ul>
<li><a href="https://www.r-project.org/help.html">Getting Help with R</a></li>
<li><a href="https://twitter.com/hashtag/rstats">#rstats Twitter</a></li>
<li><a href="https://www.rstudio.com/resources/cheatsheets/">RStudio cheat sheets</a></li>
<li>Wickham H &amp; Grolemund G (2017) <em>R for Data Science</em>: <a href="http://r4ds.had.co.nz/">ebook</a></li>
<li><a href="http://style.tidyverse.org/">Tidyverse style guide</a></li>
</ul></li>
<li>Git
<ul>
<li><a href="https://git-scm.com/docs">Git Reference</a></li>
<li><a href="https://try.github.io/">Resources to learn Git</a></li>
</ul></li>
<li>Markdown
<ul>
<li><a href="https://daringfireball.net/projects/markdown/syntax">Markdown: Syntax</a></li>
<li><a href="https://pandoc.org/MANUAL.html">Pandoc User's Guide</a></li>
</ul></li>
</ul>
<h3 id="datasets">Datasets</h3>
<ul>
<li>The diabetes data used by <a href="https://link.springer.com/article/10.1007/BF00423145">Reaven and Miller (1979)</a> can be obtained from the <a href="https://cran.r-project.org/web/packages/candisc/vignettes/diabetes.html"><strong>heplots</strong> package</a>.</li>
<li>The <a href="http://archive.ics.uci.edu/ml/index.php">UCI Machine Learning Repository</a> contains several healthcare-related datasets:
<ul>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Primary+Tumor">Primary Tumor Data Set</a>: categorical and logical variables</li>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Breast+Cancer">Breast Cancer Data Set</a>: categorical and ordinal variables</li>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Breast+Tissue">Breast Tissue Data Set</a>: categorical and continuous variables</li>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Dermatology">Dermatology Data Set</a>: discrete scale variables</li>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Hepatitis">Hepatitis Data Set</a>: logical variables</li>
<li><a href="https://archive.ics.uci.edu/ml/datasets/Heart+Disease">Heart Disease Data Set</a>: heterogeneous variables</li>
</ul></li>
<li>The National Ambulatory Medical Care Survey <a href="https://www.cdc.gov/nchs/ahcd/index.htm">at the AHRQ website</a> contains logical indicators for several chronic conditions, as well as several other variables.</li>
<li><a href="https://mimic.physionet.org/">MIMIC-III</a> is a critical care database consolidated from several sources of administrative healthcare data.</li>
</ul>
