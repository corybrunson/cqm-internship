library(tidyverse)

# load and inspect Reaven & Miller diabetes dataset
data(Diabetes, package = "heplots")
Diabetes %>%
  as_tibble() %>%
  print() ->
  diabetes

# diagnostic categories are strongly related to glucose intolerance
diabetes %>%
  ggplot(aes(x = glutest, fill = group)) +
  geom_histogram()
# less so to insulin resistance
diabetes %>%
  ggplot(aes(x = sspg, fill = group)) +
  geom_histogram()

# scatterplot of glucose tolerance and insulin resistance
diabetes %>%
  ggplot(aes(x = glutest, y = sspg, color = group)) +
  geom_point()

# principal components analysis, unscaled
diabetes %>%
  select(-group) %>%
  as.matrix() %>%
  prcomp(scale. = FALSE) %>%
  print() ->
  diabetes_pca
# default biplot
biplot(diabetes_pca, cex = .75)

# matrix multiplication retrieves scaled data
diabetes %>%
  select(-group) %>%
  as.matrix() %>%
  scale(scale = FALSE) %>%
  head()
head(diabetes_pca$x %*% t(diabetes_pca$rotation))

# color-code cases by grouping in a biplot
cols <- c("#bb000077", "#00bb0077", "#0000bb77")
#lambda <- diag(diabetes_pca$sdev[1:2])
plot(diabetes_pca$x[, 1:2] %*% diag(1 / diabetes_pca$sdev[1:2]),
     asp = 1, pch = 16,
     col = cols[as.integer(diabetes$group)])
abline(h = 0, lty = 2, col = "#00000077")
abline(v = 0, lty = 2, col = "#00000077")
points(diabetes_pca$rotation[, 1:2] %*% diag(diabetes_pca$sdev[1:2]),
       pch = 17, col = "#000000dd")
