library(tidyverse)

# read in variable names and factor levels
"primary-tumor.names" %>%
  read_lines(skip = 53, n_max = 22) %>%
  paste(collapse = " ") %>%
  str_split("[0-9]+\\.") %>%
  unlist() %>%
  str_trim() %>%
  tail(n = -1) %>%
  str_replace_all("[:space:]+", " ") %>%
  str_split(": ", simplify = TRUE) %>%
  as_tibble() %>%
  set_names(c("name", "levels")) %>%
  mutate(name = str_replace_all(name, "-", "_")) %>%
  mutate(levels = str_split(levels, ", ")) ->
  primary_tumor_names

# read in data
"primary-tumor.data" %>%
  read_csv(col_names = FALSE, na = "?") %>%
  set_names(primary_tumor_names$name) %>%
  map2_df(primary_tumor_names$levels, function(i, l) factor(l[i], l)) ->
  primary_tumor_data

# Question: The carcinomas of this dataset are grouped by their microscopic
# structure, or histology: epidermoid, which form from tissue-lining cells like
# skin; adenoidal, from glandular cells like from the pancreas; and anaplastic,
# which are rare but much more aggressive and lethal. The carcinomas in this
# dataset are distributed into these types as follows:

# visualize distribution of histologic type
primary_tumor_data %>%
  ggplot(aes(x = histologic_type)) +
  geom_bar()

# Anaplastic carcinomas are known to be less differentiated than other types.
# Between the first two types, is either consistently more differentiated?

# tabulate distribution of histologic types and amount of differentiation
primary_tumor_data %>%
  select(histologic_type, degree_of_diffe) %>%
  table() %>%
  print() ->
  tab
# check for a statistical association
chisq.test(tab)
# restrict to non-anaplastic types
primary_tumor_data %>%
  filter(histologic_type != "anaplastic") %>%
  mutate(histologic_type = fct_drop(histologic_type)) %>%
  select(histologic_type, degree_of_diffe) %>%
  table() %>%
  print() ->
  tab
chisq.test(tab)

# We find no evidence that epidermiod and adenoidal carcenomas are differently
# differentiated in these samples. The dataset also contains information on
# several unclassified carcinomas, as indicated by the `NA` in the
# `histologic_type` field. What known tumors are these most similar to?

# visualize dependence of differentiation on histologic type
primary_tumor_data %>%
  ggplot(aes(x = histologic_type, fill = degree_of_diffe)) +
  geom_bar()
# cohorted by age and gender
primary_tumor_data %>%
  filter(!is.na(sex)) %>%
  ggplot(aes(x = histologic_type, fill = degree_of_diffe)) +
  facet_grid(age ~ sex) +
  geom_bar()

# The unclassified carcinomas are most similar to the anaplastic carcinomas, the
# vast majority being poorly differentiated. However, the large number of
# adenoidal carcinomas with unrecorded differentiation must temper our
# confidence in this conclusion.

# Instead of histologic type, how are the cancers distributed according to their
# primary and metastatic sites in other organ systems?

# visualize the distribution of metastatic locations
primary_tumor_data %>%
  gather(key = "location", value = "metastasis", bone:abdominal) %>%
  drop_na() %>%
  ggplot(aes(x = location, fill = metastasis)) +
  geom_bar(position = "dodge") +
  coord_flip()

# perform correspondence analysis of site indicators
primary_tumor_data %>%
  select(-(class:degree_of_diffe)) %>%
  mutate_all(as.integer) %>%
  mutate_all(funs(. - 1L)) %>%
  drop_na() %>%
  as.matrix() %>%
  ca::ca() ->
  primary_tumor_ca
# visualize distribution across sites
plot(primary_tumor_ca)
# color-code tumors by histologic type
# (cases and variables share the inertia)
cols <- c("#bb000077", "#00bb0077", "#0000bb77")
plot(primary_tumor_ca$rowcoord[, 1:2] %*% diag(primary_tumor_ca$sv[1:2]),
     asp = 1, pch = 16,
     col = cols[as.integer(drop_na(primary_tumor_data)$histologic_type)])
abline(h = 0, lty = 2, col = "#00000077")
abline(v = 0, lty = 2, col = "#00000077")
points(primary_tumor_ca$colcoord[, 1:2] %*% diag(primary_tumor_ca$sv[1:2]),
       pch = 17, col = "#000000dd")
text(primary_tumor_ca$colcoord[, 1:2] %*% diag(primary_tumor_ca$sv[1:2]),
     cex = .75, pos = 4,
     labels = names(primary_tumor_data)[6:18])

# Metastases to the peritoneum (a membrane surrounding abdominal organs),
# abdomen, liver, mediastinum (a membrane in the thorax), bone, and lung help
# explain the most overall variation in metastatic distribution. Moreover,
# carcinomas that spread to the abdomen tend not to spread to bone, while those
# that spread to the peritoneum tend not to spread anywhere else in particular.
# The relative rarity of skin, bone marrow, and brain carcinomas prevents us
# from drawing strong conclusions about the association of metastases to these
# regions and to others. There is no clear association between metastatic
# distribution and histologic type, since carcinomas of each type are
# distributed throughout the two principal coordinates.
